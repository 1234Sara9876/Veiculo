﻿namespace Veiculo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        #region atributos

        private Veiculo carro;

        private int abastecer;
        private decimal custolt;

        #endregion

        #region construtor

        public Form1()
        {
            InitializeComponent();
            carro = new Veiculo("77-93-UV", 40.00m, 5.80m, 157235.00m, 40.00m);
            Stand();
        }

        #endregion

        #region outros métodos

        private void Viajar()
        {
            carro.Contakm = udContakm.Value;
            custolt = 1.62m;
            if (carro.CalculaKm() < carro.Contakm && carro.Deposito < carro.Capacidade && !carro.IsReserva()) 
                EstacaoServico();
            else  if (carro.IsReserva())
            {
                lblReserva.BackColor = Color.Red;
                EstacaoServico();
            }
            else
            {
                carro.Atualizar(carro.Contakm);
                udQuilometragem.Value = carro.Quilometragem;
                pbDeposito.Value = Convert.ToInt16(carro.Deposito * 100 / carro.Capacidade);

                udGastou.Value = carro.Qtcustou(custolt, carro.Contakm);
                udCustokm.Value = carro.QtcustouKm(custolt, carro.Contakm);
            }
        }

        private void Stand()
        {
            tbMatricula.Text = carro.Matricula;
            udLtsaoscem.Value = carro.Ltsaoscem;
            udCapacidade.Value = carro.Capacidade;
            pbDeposito.Value = 100;
            gb1.Enabled = true;
        }

        private void EstacaoServico()
        {
            carro.Contakm = 0;
            udAbastecer.Enabled = true;
        }

        #endregion

        private void udContakm_ValueChanged(object sender, EventArgs e)
        {
            if (udContakm.Value <= 0)
            {
                MessageBox.Show("Introduza os quilómetros do trajeto");
                return;
            }
            Viajar();
        }

        private void udAbastecer_ValueChanged(object sender, EventArgs e)
        {
            if (udAbastecer.Value <= 0)
            {
                MessageBox.Show("Introduza a quantidade de combustível");
                return;
            }
            abastecer = Convert.ToInt16(udAbastecer.Value);
            if (carro.Cheio(abastecer))
                pbDeposito.Value = 100;
            else
                pbDeposito.Value = Convert.ToInt16(carro.Deposito * 100 / carro.Capacidade);
        }
    }
}
